import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/axios'
import './plugins/bootstrap-vue'
import App from './App.vue'
import store from './store'
import router from './router'
import VueMapbox from "vue-mapbox";
import Mapbox from "mapbox-gl";
import $ from "jquery";

window.$ = window.jQuery = $;

Vue.use(VueMapbox, { mapboxgl: Mapbox });

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
