<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand logo-tab" href="{{ url('/') }}"><img src="{{ asset('images/logo.png')}}" alt=""></a>
            <mobile_location_picker v-bind:location="location"></mobile_location_picker>

            <img class="hidden-md hidden-lg search-mobile-icon" src="{{ asset('images/mobile-search.svg')}}" alt="">
            <div class="hidden-md hidden-lg search-dropdown-menu">
                <autocomplete v-bind:suggestions="suggestions" v-model="selection"></autocomplete>
            </div>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <div class="navbar-left hide-tab">
                <ul class="nav navbar-nav left-menu">
                    <location_picker v-bind:location="location"></location_picker>
                    <li class="hidden-xs search-wrapper-md">
                        <autocomplete v-bind:suggestions="suggestions" v-model="selection"></autocomplete>
                    </li>
                </ul>
            </div>
            <ul class="nav navbar-nav navbar-right user-menu">
                <li><a href="{{ url('/directory') }}">Directory</a></li>
                <li><a href="{{ url('/mapview') }}">Map View </a></li>
                @if(!\App\Helpers\Auth\AuthHelper::isLoggedIn())
                    <li><a href="javascript:void(null);" id="login-btn" data-toggle="modal" data-target="#loginModal">Sign In</a></li>
                    <li><a href="javascript:void(null);" id="login-btn" data-toggle="modal" data-target="#loginModal">Create Event</a></li>
                @else
                    
                    @if(\App\Helpers\Auth\AuthHelper::isSuperAdmin())
                        <li><a href="{{ route('admin.event.create') }}">Create Event</a></li>
                        <li class="desktop-hide"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                        @include('admin.includes._partials.user-dropdown-menu')
                    @elseif(\App\Helpers\Auth\AuthHelper::isBusinessAccount() && ! \App\Helpers\Auth\AuthHelper::isPublicAccount())
                        <li><a href="{{ route('admin.business.client.event.create') }}">Create Event</a></li>
                        <li class="desktop-hide"><a href="{{ route('admin.business.client.account') }}">My Account</a></li>
                        @include('admin.business.client._partials.user-dropdown-menu')
                    @else
                        @include('user.includes._partials.user-dropdown-menu')
                        <li class="desktop-hide"><a href="{{ route('auth.user.profile') }}">My Profile</a></li>
                        <li class="desktop-hide"><a href="{{ route('auth.user.likes') }}">Following Events</a></li>
                        <li class="desktop-hide"><a href="{{ route('auth.user.followings') }}">Following Venues</a></li>
                        <li class="desktop-hide"><a href="javascript:void(null);">Notifications</a></li>
                        
                    @endif
                    
                    
                    <li class="desktop-hide"><a href="javascript:void(0);" v-on:click="logoutUser">Logout</a></li>

                @endif
            </ul>
        </div>

    </div>
</nav>

