<nav class="navbar navbar-default navbar-fixed-top darkHeader inner-darkHeader">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand logo-tab" href="{{ url('/') }}"><img src="{{ asset('images/logo.png')}}" alt=""></a>

                <mobile_location_picker v-bind:location="location"></mobile_location_picker>

                <img class="hidden-md hidden-lg search-mobile-icon" src="{{ asset('images/mobile-search.svg')}}">
                <div class="hidden-md hidden-lg search-dropdown-menu">
                <autocomplete v-bind:suggestions="suggestions" v-model="selection"></autocomplete>
                </div>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <div class="navbar-left hide-tab">
                    <ul class="nav navbar-nav left-menu">
                        <location_picker v-bind:location="location"></location_picker>
                        <li class="hidden-xs">
                            <autocomplete v-bind:suggestions="suggestions" v-model="selection"></autocomplete>
                        </li>
                    </ul>
                </div>
                <ul class="nav navbar-nav navbar-right user-menu">
                    @if(\App\Helpers\Auth\AuthHelper::isLoggedIn())
                        <li class="desktop-hide"><a href="{{ route('auth.user.profile') }}">My Profile</a></li>
                        @if (\Request::is('directory'))
                            <li class="active-menu"><a href="{{ url('/directory') }}">Directory</a></li>
                        @else
                            <li><a href="{{ url('/directory') }}">Directory</a></li>
                        @endif
                        @if (\Request::is('mapview'))
                            <li class="active-menu"><a href="{{ url('/mapview') }}">Map View</a></li>
                        @else
                        <li><a href="{{ url('/mapview') }}">Map View</a></li>
                        @endif
                        <li class="desktop-hide"><a href="{{ route('auth.user.likes') }}">Likes</a></li>
                        <li class="desktop-hide"><a href="{{ route('auth.user.followings') }}">Followings</a></li>
                        <li class="desktop-hide"><a href="javascript:void(null);">Notifications</a></li>
                        <li class="desktop-hide"><a href="javascript:void(0);" v-on:click="logoutUser">Logout</a></li>
                        {{--<li class="desktop-hide"><a href="{{ route('logout') }}">Logout</a></li>--}}
                    @endif


                    @if(!\App\Helpers\Auth\AuthHelper::isLoggedIn())
                    @if (\Request::is('directory'))
                    <li class="active-menu"><a href="{{ url('/directory') }}">Directory</a></li>
                        @else
                            <li><a href="{{ url('/directory') }}">Directory</a></li>
                        @endif
                        @if (\Request::is('mapview'))
                            <li class="active-menu"><a href="{{ url('/mapview') }}">Map View</a></li>
                        @else
                        <li><a href="{{ url('/mapview') }}">Map View</a></li>
                        @endif
                        <li><a href="javascript:void(null);" id="login-btn" data-toggle="modal" data-target="#loginModal">Sign In</a></li>
                        <li><a href="javascript:void(null);" id="login-btn" data-toggle="modal" data-target="#loginModal">Create Event</a></li>
                    @else
                            @if(\App\Helpers\Auth\AuthHelper::isSuperAdmin())
                                <li><a href="{{ route('admin.event.create') }}">Create Event</a></li>
                                @include('admin.includes._partials.user-dropdown-menu')
                            @elseif(\App\Helpers\Auth\AuthHelper::isBusinessAccount()  && ! \App\Helpers\Auth\AuthHelper::isPublicAccount())
                                <li><a href="{{ route('admin.business.client.event.create') }}">Create Event</a></li>
                                @include('admin.business.client._partials.user-dropdown-menu')
                            @else
                                @include('user.includes._partials.user-dropdown-menu')
                            @endif

                    @endif


                </ul>
            </div>
    
        </div>
    </nav>
    
    