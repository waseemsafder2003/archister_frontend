<div class="menu-box">
    <ul>
        <li><a class="{{ (request()->is('faqs')) ? 'active' : '' }}"   href="{{ route('frontend.info.fqas.page')  }}">Knowledge Base</a></li>
        <li><a class="{{ (request()->is('contact-support')) ? 'active' : '' }}" href="{{ route('frontend.info.contact.support')  }}">Contact Support</a></li>
    </ul>
</div>