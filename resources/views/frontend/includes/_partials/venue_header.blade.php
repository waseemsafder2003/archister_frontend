<section class="my-venue-view">
        <div class="container">
    <div class="row">
        <div class="col-md-3">
          <div class="venue-logo">
            <img src="{{$eventDetail->image}}">
          </div>  
          <div class="venue-datess">
            <div>
              <p class="blue">Full week schedule</p>
              <p>Monday - Friday</p>
              <p>Saturday</p>
              <p>Sunday</p>
            </div>
            <div>
              <p>&nbsp;</p>
              <p>11am - 2am</p>
              <p>12pm - 4am</p>
              <p>12pm - 2am</p>
            </div>
          </div>
        </div>
        <div class="col-md-6">
           <div class="col-md-12 no-padding-l-r">
          <h2>{{$eventDetail->title}}</h2>
          <a class="follow mobile-hide" href="javascript:void(null)">
            <img src="{{asset('images/fav-w.svg')}}"> 
            Follow
          </a>
          <a class="share mobile-hide" href="javascript:void(null)"><img src="{{asset('images/share.svg')}}"> Share</a>
          <span class="foll">Followers: 78455</span>
        </div>
        <div class="col-md-6 no-padding-l-r">
            <div class="venue-dates">
                @if($eventDetail->venue[0]->age ||  $eventDetail->venue[0]->attire)
                <div>
                    <p class="blue">Amenities</p>
                    <p>Age Limit</p>
                    <p>Attire</p>
                    <p>Food</p>
                    <p>Wi-Fi</p>
                  </div>
                  <div>
                    <p>&nbsp;</p>
{{--                    <p>{{$eventDetail->venue[0]->age}}</p>--}}
                    <p>{{$eventDetail->venue[0]->attire}}</p>
                    <p>{{$eventDetail->venue[0]->food ? "yes" : "no"}}</p>
                    <p>{{$eventDetail->venue[0]->wifi  ? "yes" : "no"}}</p>
                  </div>
                @endif
            </div>
            <div class="venue-dates">
              <div>
                <p class="blue">Bottle Service/ VIP</p>
                <p>Timothy Watson</p>
                <p>vip@lakewood.com</p>
                <p>+1 999-179-3481</p>
              </div>
            </div>
          </div>
          <div class="col-md-6 no-padding-l-r">
            @if($eventDetail->social_contacts)
              <div class="venue-dates">
                <div>
                  <p class="blue">Join Us</p>
                  <ul class="social-icons">
                      @foreach ($eventDetail->social_contacts as $socialIcons)
                      <li>
                        <a href="{{$socialIcons->value}}" target="_blank">
                          @if($socialIcons->provider == 'facebook') 
                          <img src="{{asset('images/fb-2.svg')}}">
                          @elseif($socialIcons->provider == 'youtube')
                          <img src="{{asset('images/youtube.svg')}}">
                          @elseif($socialIcons->provider == 'website')
                          <img src="{{asset('images/website.svg')}}">
                          @elseif($socialIcons->provider == 'phone')
                          <img src="{{asset('images/phone.svg')}}">
                          @elseif($socialIcons->provider == 'twitter')
                          <img src="{{asset('images/twt-2.svg')}}">
                          @endif
                        </a>
                      </li>    
                    @endforeach
                </ul>
                </div>
              </div>
            @endif
            @foreach($eventDetail->contacts as $contact)
              <div class="venue-dates">
                <div>
                <p class="blue">{{ ucfirst($contact->provider) }}</p>
                <p>{{$contact->value}}</p>
                </div>
              </div>
            @endforeach
          </div>  
        </div>
        @if($eventDetail->venue[0]->address_details && @$eventDetail->venue[0]->address_details->latitude && $eventDetail->venue[0]->address_details->longitude)
          <div class="col-md-3">
              <iframe src="https://maps.google.com/maps?q={{$eventDetail->venue[0]->address_details->latitude}},{{$eventDetail->venue[0]->address_details->longitude}}&hl=es;z=14&amp;output=embed" width="296" height="296" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            <div class="map-address">
              <p>{{$eventDetail->venue[0]->address_details->street}}&nbsp;{{$eventDetail->venue[0]->address_details->city}}&nbsp;{{$eventDetail->venue[0]->address_details->zip_code}}</p>
              <p class="km">3.2 km</p>
            </div>
            <ul>
            {{-- <li>
              <a class="direction" href="javascript:void(null)"><img src="{{asset('images/direction.svg')}}"> Direction</a>
            </li> --}}
          </ul>
          </div>
        @endif
      </div>
    </div>
  </section>  