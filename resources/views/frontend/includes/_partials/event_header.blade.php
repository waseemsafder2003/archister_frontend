<section class="event-detail">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="event-detail-flyer-img-wrapper">
                    @if($eventDetail->image)
                        <img class="cris" src="{{$eventDetail->image}}" alt="image">
                    @endif
                </div>
            </div>
            <div class="col-md-8">
                <h2>{{$eventDetail->title}}</h2>
                <div class="event-dates">
                    <div class="left">
                        <img src="{{asset('images/info.svg')}}">
                    </div>
                    <div class="center">
                   
                        <div class="vgps-event-details-row">
                            <div class="vgps-event-detail-label">Start Date</div>
                            <div class="vgps-event-detail-desc">{{$eventDetail->formatted_date_time->start_date}}</div>
                        </div>
                        <div class="vgps-event-details-row">
                            <div class="vgps-event-detail-label">End Date</div>
                            <div class="vgps-event-detail-desc">{{$eventDetail->formatted_date_time->end_date}}</div>
                        </div>

                        <div class="vgps-event-details-row">
                            <div class="vgps-event-detail-label">Doors Open at</div>
                            <div class="vgps-event-detail-desc">{{ $eventDetail->formatted_date_time->door_open?$eventDetail->formatted_date_time->door_open:'N/A' }}</div>
                        </div>
                        <div class="vgps-event-details-row">
                            <div class="vgps-event-detail-label">Starts at</div>
                            <div class="vgps-event-detail-desc">{{$eventDetail->formatted_date_time->start_time}}</div>
                        </div>

                        @if(!empty($eventDetail->days))
                            <div class="vgps-event-details-row">
                                <div class="vgps-event-detail-label">Days</div>
                                <div class="vgps-event-detail-desc">
                                    
                                </div>
                            </div>
                            <ul class="recurring-days-list">
                                @foreach($eventDetail->days as $day)
                                    <li>{{ $day }}</li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
    
                @if(!empty($eventDetail->hashtags))
                <div class="event-dates">
                    <div class="left">
                        <img src="{{asset('images/tag.svg')}}">
                    </div>
                    <div class="center">
                        <div class="vgps-event-details-row">
                            <div class="vgps-event-detail-desc vgps-event-detail-header-tags-wrapper">
                            
                                    <ul class="vgps-event-detail-header-tags">
                                        <!-- <li class="tag-item"><a href="{{ route('frontend.home.index',['hashtag' => '#'.$eventDetail->category ]) }}">{{ '#'.$eventDetail->category }}</a></li> -->
                                        <!-- <li class="tag-item"><a href="{{ route('frontend.home.index',['hashtag' => '#'.$eventDetail->type ]) }}">{{ '#'.$eventDetail->type }}</a></li> -->
                                        <!-- <li class="tag-item"><a href="{{ route('frontend.home.index',['hashtag' => '#'.$eventDetail->theme ]) }}">{{ '#'.$eventDetail->theme }}</a></li> -->
                                        
                                            @foreach($eventDetail->hashtags as $tag)
                                                <li class="tag-item"><a href="{{ route('frontend.home.index',['hashtag' => $tag->title]) }}">{{ $tag->title }}</a></li>
                                            @endforeach
                                        
                                    </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @endif


           

                <div class="event-dates">
                    <div class="left">
                        <img src="{{asset('images/pin.svg')}}">
                    </div>
                    <div class="center">
                        <p>
                            <?php 

                                // dd($eventDetail);
                                $url = '';
                                if(!empty($eventDetail->venue)){
                                    $url = url('venue-detail/'.$eventDetail->venue[0]->id);
                                    $businessCode = $eventDetail->venue[0]->business_type_code;
                                    if($businessCode == 'people'){
                                        $url = url('people/'.$eventDetail->venue[0]->id);     
                                    } else if($businessCode == 'community'){
                                        $url = url('community/'.$eventDetail->venue[0]->id);
                                    }?>   
                                <a href="{{ $url}}">{{$eventDetail->venue[0]->title}}</a><br/>
                                <?php } ?>
                            

                        {{ \App\Helpers\General\DataHelper::getFormattedAddress($eventDetail->address_details) }}
                        
                        </p>
                    </div>
                </div>
                <?php //dd($eventDetail) ?>
                <div class="event-btns vgps-events-btns">

                    <?php $follow = ($eventDetail->follow === false) ?  'false': 'true'  ?>
                    <event_detail_follow_button
                            v-bind:id="{{ $eventDetail->id}}"
                            v-bind:title="`{{ $eventDetail->title}}`"
                            v-bind:follow="'{{ $follow }}'"></event_detail_follow_button>                

                    @if(!\App\Helpers\Auth\AuthHelper::isLoggedIn())
                        <button class="" href="javascript:void(null);" data-toggle="modal" data-target="#loginModal">
                            <img src="{{asset('images/heart-grey.svg')}}">
                            Like {{ $eventDetail->likes_count }}</button>
                    @else
                        <?php $class = $eventDetail->like == true ? 'pink' : 'class' ?>
                        <button class="not-liked {{ $class }}" href="javascript:void(null);"
                                data-liked="{{ $eventDetail->like }}"
                                data-liked-count="{{ $eventDetail->favourites_count }}"
                                onclick="markLikeEvent('<?php echo $eventDetail->id?>', this);">
                            <?php if($eventDetail->like == true){?>
                            <img src="{{asset('images/heart.svg')}}" alt="">
                            <?php } else{
                            ?>
                            <img src="{{asset('images/heart-grey.svg')}}" alt="">
                            <?php
                            }
                            ?>
                            Like <i class="favourites-count">{{ $eventDetail->likes_count }}</i></button>
                    @endif

                    
                    <button style="cursor: unset;" href="javascript:void(null);"><img
                                src="{{asset('images/seen.svg')}}" alt=""> {{ $eventDetail->total_views }}</button>
                    <span class="shareBtnDetail" shareCount="{{$eventDetail->share_count}}"
                          id="event_{{$eventDetail->id}}"></span>
                    {{-- <a href="#"><img src="{{asset('images/share.svg')}}"> {{ $eventDetail->share_count }}</a> --}}
                    {{-- <span class="shareBtnDetail" id="event_{{$eventDetail->id}}" title="{{$eventDetail->title}}"></span> --}}
                    {{-- <a class="buy-btn" href="#">Buy Ticket</a> --}}

                    <?php //dd($eventDetail)?>
                </div>
            </div>
        </div>
    </div>
</section>