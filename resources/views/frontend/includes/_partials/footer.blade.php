
<!-- Footer Start -->
<footer class="page-footer">
    <section class="top-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <h6>Information</h6>
                    <ul>
                        <li><a href="{{ route('frontend.info.contact.support') }}">Contact Us</a></li>
                        <li><a href="{{ route('frontend.info.fqas.page') }}">FAQ’s</a></li>
                        <li><a href="{{ route('frontend.info.privacy.policy.page') }}">Privacy Policy</a></li>
                        <li><a href="{{ route('frontend.info.term.page') }}">Terms of Use</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <h6>Advertise with Us</h6>
                    <ul>
                        <li><a href="#">Advertising Guidelines</a></li>
                        <li><a href="#">Why Us?</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 m-t-m clear-left">
                    <h6>Media</h6>
                    <ul>
                        <li><a href="#">Press Kit</a></li>
                        <li><a href="#">Press Catalog</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-6 m-t-m">
                    <h6>Company</h6>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Team</a></li>
                        <li><a href="#">Partnerships</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="bottom-footer">
        <div class="container">
            <div class="row border-top">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <p>&copy; <?= date('Y') ?> VenueGPS</p>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <ul class="social-icons">
                        <li><a href="https://www.facebook.com/VENUEGPS/" target="_blank"><img src="{{ asset('images/fb.svg')}}" /></a></li>
                        <li><a href="https://twitter.com/venuegps" target="_blank"><img src="{{ asset('images/twt.svg')}}" /></a></li>
                        <li><a href="https://www.instagram.com/venuegps/" target="_blank"><img src="{{ asset('images/insta.svg')}}"  /></a></li>
                        <li><a href="https://www.youtube.com/channel/UCaQ-SvgfP9_-b86k60BmUcQ/videos" target="_blank"><img src="{{ asset('images/youtube.svg')}}"  /></a></li>
                        <li><a href="https://snapchat.com/add/venuegps" target="_blank"><img src="{{ asset('images/snapchat.svg')}}"  /></a></li>
                        {{-- <li><a href="#"><img src="{{ asset('images/spotify.svg')}}" target="_blank" /></a></li>
                        <li><a href="#"><img src="{{ asset('images/blog.svg')}}" target="_blank" /></a></li> --}}
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <input id="calendarStart_date" value="" type="hidden">
    <input id="calendarEnd_date" value="" type="hidden">
    <input id="selectedStart_date" value="" type="hidden">
    <input id="selectedEnd_date" value="" type="hidden">
</footer>
<!-- Footer End -->

