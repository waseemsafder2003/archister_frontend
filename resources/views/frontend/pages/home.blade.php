@extends('frontend._layouts.app')
{{-- @section('title', app_name() . ' | ' . __('frontend.page.title.home')) --}}
@section('title', __('frontend.page.title.home'))

@section('body_class','home-page')

@section('top_nav')
    @include('frontend.includes._partials.nav')
@endsection
@section('page_banner')
    @include('frontend.includes.banners.home')
@endsection

@section('content')

    <div :class="{ hide : hidePageLoading }" class="pageloader content-loader" v-animate-css="animate.fadeOut">
        <img :src="'loader-gif.gif' | assetPath">
        <p>Loading Contents ...</p>
    </div>

    <div v-if="hidePageLoading" class="home-vue-content" v-animate-css="animate.slideInRight">
        <home_content v-bind:location="location" v-bind:hashtag="'{{ request('hashtag') }}'" v-bind:lookupData="lookupData" v-bind:userData="userData"></home_content>
    </div>

@endsection


