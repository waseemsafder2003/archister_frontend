<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="base_url" content="{{ url('/') }}">

    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.png') }}"/>
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/favicon.png') }}"/>

    <title>@yield('title', app_name())</title>


    <meta property="og:title" content="VenueGPS"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="@yield('meta_description', 'VenueGPS')">
    <meta name="author" content="@yield('meta_author', '')">
@yield('meta')

@stack('before-styles')

<!-- Check if the language is set to RTL, so apply the RTL layouts -->

<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Poppins:400,500,600,700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
   
    @stack('after-styles')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCgsWtBQ7-GDlxPQ8k9i_fNAN1b1C5-rq4&libraries=places"></script>

</head>
<body class="@yield('body_class')">
<div id="app">
    <header>
        @yield('top_nav')
    </header>
@yield('page_banner')
@yield('content')
@include('frontend.includes._partials.footer')
@include('frontend.includes._partials.modals')
<!-- Footer -->
</div><!-- #app -->



<!-- Before Scripts -->
@stack('before-scripts')

<script type="text/javascript" src="{{ asset(mix('js/app.js')) }}"></script>
{{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAanAhiuaASZMW3t83xTcb2EHE_d_8vi3s&libraries=places" async="" defer=""></script>--}}
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<!-- Loading jQuery Equal Height for Newer version of Jquery -->
<script type="text/javascript" src="{{ asset('js/jquery-equal-height.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/custom.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/plugin.js')}}"></script>
<script type="text/javascript" src=" {{ asset('js/owl.carousel.min.js')}}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="{{ asset('js/date-picker.js')}}"></script>

<!-- After Scripts -->
@stack('after-scripts')
</body>
</html>
