<?php

namespace App\Http\Resources;
   
use Illuminate\Http\Resources\Json\JsonResource;
   
class PropertySummery extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'suburb' => $this->suburb,
            'state' => $this->state,
            'country' => $this->country,
            'min_value' => $this->min_value,
            'max_value' => $this->max_value,
            'median_value' => $this->median_value,
        ];
    }
}