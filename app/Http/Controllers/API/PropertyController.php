<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use App\Models\PropertyModel as Property;
use App\Http\Resources\Property as PropertyResource;

class PropertyController extends BaseController
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $property = Property::all();
        return $this->sendResponse(PropertyResource::collection($property), 'Properties retrieved successfully.');
    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getGeoData()
    {
        $filename = 'properties-geo-data.json';
        // Read File
        $jsonString = file_get_contents(base_path('resources/data/'.$filename));
        $data = json_decode($jsonString, true);
        return $this->sendResponse($data, 'Properties analytic geo data retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'suburb' => 'required',
            'state' => 'required',
            'country' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $property = Property::create($input);
        return $this->sendResponse(new PropertyResource($property), 'Property created successfully.');
    } 
   
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $property = Property::find($id);
  
        if (is_null($property)) {
            return $this->sendError('Property not found.');
        }
   
        return $this->sendResponse(new PropertyResource($property), 'Property retrieved successfully.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Property $property)
    {
        $input = $request->all();
   
        // dd($input,$property);
        $validator = Validator::make($input, [
            'suburb' => 'required',
            'state' => 'required',
            'country' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
   
        $property->suburb = $input['suburb'];
        $property->state = $input['state'];
        $property->country = $input['country'];
        $property->save();
   
        return $this->sendResponse(new PropertyResource($property), 'Property updated successfully.');
    }
   
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Property $property,$id)
    {

        $property = Property::find($id);
        if (is_null($property)) {
            return $this->sendError('Property not found.');
        }

        $property->delete();
   
        return $this->sendResponse([], 'Property deleted successfully.');
    }
}
