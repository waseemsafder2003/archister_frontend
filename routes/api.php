<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\PropertyController; 
use App\Http\Controllers\API\PropertyAnalyticController; 
use App\Http\Controllers\API\PropertySummeryController; 

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register', 'API\RegisterController@register');
Route::post('login', 'API\RegisterController@login');

// Route::group(['middleware' => 'auth:api'], function () {
    
    // Route::group(['prefix' => 'v1/','as' => 'v1.'], function () {
            
        Route::group(['prefix' => 'properties/','as' => 'properties.'], function () {    
            
            Route::get('/', [PropertyController::class,'index'])->name('all');
            Route::get('/geo-data', [PropertyController::class,'getGeoData'])->name('geo.data');
            Route::get('{id}', [PropertyController::class,'detail'])->name('detail');
            Route::post('/', [PropertyController::class,'store'])->name('store');
            Route::put('{id}', [PropertyController::class,'update'])->name('update');
            Route::delete('{id}', [PropertyController::class,'destroy'])->name('destroy');

            Route::group(['prefix' => '{pid}/analytics/','as' => 'analytics.'], function () {
                Route::post('/add', [PropertyAnalyticController::class,'store'])->name('store');
                Route::get('/', [PropertyAnalyticController::class,'index'])->name('all');
                Route::get('{id}/', [PropertyAnalyticController::class,'detail'])->name('detail');
                Route::put('{id}', [PropertyAnalyticController::class,'update'])->name('update');
                Route::delete('{id}', [PropertyAnalyticController::class,'destroy'])->name('destroy');
            });
            
            Route::group(['prefix' => 'summery/{col}/','as' => 'summery.'], function () {
                Route::get('{val}/', [PropertySummeryController::class,'index'])->name('all');
            }); 
           
        }); 


    // });
// });
